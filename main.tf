terraform {
  backend "http" {
  }
}


provider "aws" {
  region = "us-west-2"
}

locals {
  cluster_instance_count = var.cluster_size == 1 #module.this.enabled # ? 
  is_regional_cluster    = var.cluster_type == "regional"
}

# The name "primary" is poorly chosen. We actually mean standalone or regional.
# The primary cluster of a global database is actually created with the "secondary" cluster resource below.
resource "aws_rds_cluster" "primary" {
  # count                               = module.this.enabled && local.is_regional_cluster ? 1 : 0
  cluster_identifier      = var.cluster_identifier
  database_name           = var.db_name
  master_username         = var.admin_user
  master_password         = var.admin_password
  backup_retention_period = var.retention_period
  # preferred_backup_window             = var.backup_window
  # copy_tags_to_snapshot               = var.copy_tags_to_snapshot
  #   final_snapshot_identifier           = var.cluster_identifier == "" ? lower(module.this.id) : lower(var.cluster_identifier)
  #   skip_final_snapshot                 = var.skip_final_snapshot
  apply_immediately = var.apply_immediately
  storage_encrypted = var.engine_mode == "serverless" ? null : var.storage_encrypted
  #kms_key_id                          = var.kms_key_arn
  source_region = var.source_region
  #   snapshot_identifier                 = var.snapshot_identifier
  vpc_security_group_ids       = var.vpc_security_group_ids
  preferred_maintenance_window = var.maintenance_window
  db_subnet_group_name         = var.db_subnet_group_name
  # db_cluster_parameter_group_name     = join("", aws_rds_cluster_parameter_group.default.*.name)
  iam_database_authentication_enabled = var.iam_database_authentication_enabled
  #tags                                = var.tags
  engine         = var.engine
  engine_version = var.engine_version
  engine_mode    = var.engine_mode
  port           = var.port
  #   iam_roles                           = var.iam_roles
  #   backtrack_window                    = var.backtrack_window
  # enable_http_endpoint                = var.engine_mode == "serverless" && var.enable_http_endpoint ? true : false

}



resource "aws_rds_cluster_instance" "default" {
  #count                           = 2
  identifier              = var.cluster_identifier
  cluster_identifier      = aws_rds_cluster.primary.cluster_identifier
  instance_class          = var.instance_type
  db_subnet_group_name    = var.db_subnet_group_name
  db_parameter_group_name = var.db_parameter_group_name
  publicly_accessible     = var.publicly_accessible
  #tags                            = var.tags
  engine         = var.engine
  engine_version = var.engine_version
  #   auto_minor_version_upgrade      = var.auto_minor_version_upgrade
  // monitoring_interval             = var.rds_monitoring_interval
  // monitoring_role_arn             = var.enhanced_monitoring_role_enabled ? join("", aws_iam_role.enhanced_monitoring.*.arn) : var.rds_monitoring_role_arn
  performance_insights_enabled = var.performance_insights_enabled
  #   performance_insights_kms_key_id = var.performance_insights_kms_key_id
  availability_zone = var.instance_availability_zone

}


resource "aws_rds_cluster_instance" "default2" {
  #count                           = 2
  identifier              = var.cluster_identifier2
  cluster_identifier      = aws_rds_cluster.primary.cluster_identifier
  instance_class          = var.instance_type
  db_subnet_group_name    = var.db_subnet_group_name
  db_parameter_group_name = var.db_parameter_group_name
  publicly_accessible     = var.publicly_accessible
  #tags                            = var.tags
  engine         = var.engine
  engine_version = var.engine_version
  #   auto_minor_version_upgrade      = var.auto_minor_version_upgrade
  // monitoring_interval             = var.rds_monitoring_interval
  // monitoring_role_arn             = var.enhanced_monitoring_role_enabled ? join("", aws_iam_role.enhanced_monitoring.*.arn) : var.rds_monitoring_role_arn
  performance_insights_enabled = var.performance_insights_enabled
  #   performance_insights_kms_key_id = var.performance_insights_kms_key_id
  availability_zone = var.instance_availability_zone

}

# ################################################################################
# # Replica DB
# ################################################################################

# resource "aws_rds_cluster_instance" "read_replica" {
#   identifier = "read-replica"
#   cluster_identifier  = aws_rds_cluster.primary.cluster_identifier
#   # Source database. For cross-region use this_db_instance_arn
#   #replicate_source_db = "prithvisamplecluster"

#   engine               = var.engine
#   engine_version       = var.engine_version
#   # family               = var.family
#   # major_engine_version = var.major_engine_version
#   instance_class       = var.instance_type

#   # allocated_storage     = var.allocated_storage
#   # max_allocated_storage = var.max_allocated_storage
#   storage_encrypted     = false

#   // # Username and password should not be set for replicas
#   // username = ""
#   // password = ""
#   port = var.port

#   multi_az               = true
#   subnet_ids             = aws_rds_cluster_instance.default.db_subnet_group_name
#   vpc_security_group_ids = [aws_rds_cluster_instance.default.this_security_group_id]

#   maintenance_window              = "Tue:00:00-Tue:03:00"
#   backup_window                   = "03:00-06:00"
#   enabled_cloudwatch_logs_exports = ["postgresql", "upgrade"]

#   backup_retention_period = 0
#   skip_final_snapshot     = true
#   deletion_protection     = false

#   # Not allowed to specify a subnet group for replicas in the same region
#   create_db_subnet_group = false

#   // tags = .tags
# }

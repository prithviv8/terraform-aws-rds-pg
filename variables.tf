variable "tags" {
  type        = string  
  default     = "prithvisamplecluster1"
}

variable "storage_encrypted" {
  type        = bool
  default     = false
}

variable "cluster_identifier" {
  type        = string
  default     = "prithvisamplecluster"
}

variable "cluster_identifier2" {
  type        = string
  default     = "prithvisampleclusterreader"
}

variable "major_engine_version" {
  type        = number
  default     = 12
}

variable "major_engine_vallocated_storage" {
  type        = number
  default     = 1
}

variable "max_allocated_storage" {
  type        = number
  default     = 2
}

variable "port" {
  type        = number
  default     = 5432
}

variable "family" {
  type        = string
  default     = "postgres12"
}

variable "db_name" {
  type        = string
  default     = "prithvisampledb"
  }

variable "admin_user" {
  type        = string
  default     = "pavan"
  description = "(Required unless a snapshot_identifier is provided) Username for the master DB user"
}

variable "admin_password" {
  type        = string
  default     = "admin1234" 
}

variable "retention_period" {
  type        = number
  default     = 5
}

variable "apply_immediately" {
  type        = bool
  default     = true
}

variable "engine_mode" {
  type        = string
  default     = "provisioned"
}

# variable "kms_key_arn" {
#   type        = string
#   default     = ""
# }

variable "source_region" {
  type        = string
  default     = "us-west-2"
}

variable "vpc_security_group_ids" {
  type        = list(string)
  default = []
}

variable "maintenance_window" {
  type        = string
  default     = "wed:03:00-wed:04:00"
}

variable "iam_database_authentication_enabled" {
  type        = bool
  description = "Specifies whether or mappings of AWS Identity and Access Management (IAM) accounts to database accounts is enabled"
  default     = false
}

variable "db_subnet_group_name" {
  type        = string
  default     = ""
}

variable "db_parameter_group_name" {
  type        = string
  default     = "default.aurora-postgresql12"
}

variable "engine" {
  type        = string
  default     = "aurora-postgresql"
}

variable "engine_version" {
  type        = number
  default     = 12.4
}


variable "cluster_size" {
  type        = number
  default     = 1
  description = "Number of DB instances to create in the cluster"
}

variable "instance_type" {
  type        = string
  default     = "db.r5.large"
}

variable "publicly_accessible" {
  type        = bool
  default     = false
}

variable "performance_insights_enabled" {
  type        = bool
  default     = false
}

variable "iam_roles" {
  type        = list(string)
  default     = ["RDSCICD"]
}

variable "instance_availability_zone" {
  type        = string
  default     = "us-west-2a"
}


variable "cluster_type" {
  type        = string
  description = <<-EOT
    Either `regional` or `global`.
    If `regional` will be created as a normal, standalone DB.
    If `global`, will be made part of a Global cluster (requires `global_cluster_identifier`).
    EOT
  default     = "regional"

  validation {
    condition     = contains(["regional", "global"], var.cluster_type)
    error_message = "Allowed values: `regional` (standalone), `global` (part of global cluster)."
  }
}
